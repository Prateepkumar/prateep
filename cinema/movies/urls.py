from django.conf.urls import url
from movies.views import movieview
from movies.views import movierev
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
	url(r'^v1/get_movie/$',csrf_exempt(movieview.as_view())),
	url(r'^v1/get_rev/$',csrf_exempt(movierev.as_view())),

]