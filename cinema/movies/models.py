from __future__ import unicode_literals

from django.db import models

# Create your models here.
class movie(models.Model):
    actor = models.CharField(max_length=50)
    movie_name= models.CharField(max_length=30)

class song(models.Model):
 	movie = models.ForeignKey(movie, on_delete=models.CASCADE)
 	file_type=models.CharField(max_length=40)
 	
class movie_review(models.Model):
	movie_id=models.ForeignKey(movie,on_delete=models.CASCADE)
	movie_rating=models.IntegerField(default=0)