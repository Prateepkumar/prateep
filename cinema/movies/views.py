from django.http import HttpResponse,QueryDict
from django.core.exceptions import ObjectDoesNotExist
#def index(request):
#return HttpResponse("hello friends how are you")
from django.views.generic import View
import json
from movies.models import movie,song,movie_review
class movieview(View):
    def get(self,request):
        _params=request.GET
        data=[]
        #p=_params.get('id')
        #my=movie.objects.filter(actor="salman")
        my=movie.objects.all()  #using all method
        for i in my:
            data.append({
                'actor':i.actor,
                'movie_name':i.movie_name
            })
       #     return HttpResponse(data,content_type='application/json')
        return HttpResponse(json.dumps(data),content_type='application/json')

    def post(self, request):
        data=request.POST
        q=movie(actor=data.get('actor'),movie_name=data.get('movie_name'))
        q.save()
        return HttpResponse()
    def put(self,request):
        data=QueryDict(request.body)
        p=movie.objects.get(id=data.get('id'))
        p.actor=data.get('actor')
        p.movie_name=data.get('movie_name')
        p.save()
        return HttpResponse()

class movierev(View):
    def get(self,request):
        _params=request.GET
        data=[] #using method of get by particular id
        
        i=movie_review.objects.get(movie_id=_params.get('id'))
        data.append({
            'movie_id':i.movie_id_id,
            'movie_rating':i.movie_rating
            })
        return HttpResponse(json.dumps(data),content_type='application/json')

    def post(self,request):
        data=request.POST
        try:
            p=movie.objects.get(id=data.get('movie_id'))
        except movie.DoesNotExist:
            return HttpResponse(" object does not exist")
            q=movie_review(movie_id=p,movie_rating=data.get('movie_rating'))
            q.save()
            print q.__dict__
            return HttpResponse()


 # movie_id = movie(object)
 # movie_id_id = int(movie_id)