from django.contrib import admin
from .models import movie,song,movie_review
admin.site.register(movie)
admin.site.register(song)
admin.site.register(movie_review)

# Register your models here.
