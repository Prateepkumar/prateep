from django.apps import AppConfig


class BookTicketConfig(AppConfig):
    name = 'book_ticket'
