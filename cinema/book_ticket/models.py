from __future__ import unicode_literals

from django.db import models
from movies.models import movie
class ticket(models.Model):
	movie=models.ForeignKey(movie,on_delete=models.CASCADE)
	no_of_ticket=models.IntegerField(default=1)