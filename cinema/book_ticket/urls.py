from django.conf.urls import url
from book_ticket.views import ticketview
from django.views.decorators.csrf import csrf_exempt
urlpatterns = [
	url(r'^v1/book_ticket/$',csrf_exempt(ticketview.as_view())),
]