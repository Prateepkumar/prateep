from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
import json
from django.db import models
from book_ticket.models import ticket
from movies.models import movie


# Create your views here.
class ticketview(View):
	def get(self,request):
		_params=request.GET
		data=[] # using exclude method
		my=ticket.objects.exclude(movie=_params.get('id'))
		for i in my:
			data.append({
				'movie':i.movie_id,
				'no_of_ticket':i.no_of_ticket
				})
		return HttpResponse(json.dumps(data),content_type='application/json')
        def post(self,request):
        	data=request.POST
        	try:
        		p=movie.objects.get(id=data.get('movie_id'))
        	except movie.DoesNotExist:
        		return HttpResponse("object for this movie does not exist")
        		q=ticket(movie=p,no_of_ticket=data.get('no_of_ticket'))
        		q.save()
        		print q.__dict__
        		return HttpResponse()